import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:k24/model/login.dart';
import 'package:k24/setting.dart';

class User {
  User({
    this.partnerId,
    this.partnerCode,
    this.name,
    this.description,
  });

  String partnerId;
  String partnerCode;
  String name;
  String description;

  factory User.fromRawJson(String str) => User.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory User.fromJson(Map<String, dynamic> json) => User(
        partnerId: json["partnerID"] == null ? null : json["partnerID"],
        partnerCode: json["partnerCode"] == null ? null : json["partnerCode"],
        name: json["name"] == null ? null : json["name"],
        description: json["description"] == null ? null : json["description"],
      );

  static Future<User> postLogin(LoginApi dataLogin) async {
    final apiUrl = Uri.parse('$IP_SERVER/member/login');
    print(dataLogin.nama);
    print(dataLogin.password);
    final data = await http.post(apiUrl, body: {
      "username": "${dataLogin.nama}",
      "password": "${dataLogin.password}"
    });
    var jsonObject = jsonDecode(data.body);
    print(jsonObject);
    var getData = jsonObject['user'];
    print("ddddd");
    print(getData['partnerID']);
    return User(
      partnerId: getData["partnerID"] == null ? null : getData["partnerID"],
      partnerCode:
          getData["partnerCode"] == null ? null : getData["partnerCode"],
      name: getData["name"] == null ? null : getData["name"],
      description:
          getData["description"] == null ? null : getData["description"],
    );
  }

  Map<String, dynamic> toJson() => {
        "partnerID": partnerId == null ? null : partnerId,
        "partnerCode": partnerCode == null ? null : partnerCode,
        "name": name == null ? null : name,
        "description": description == null ? null : description,
      };
}
