import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:k24/model/user.dart';
import 'package:k24/setting.dart';

class Member {
  Member({
    this.id,
    this.name,
    this.dateOfBirth,
    this.registered,
  });

  String id;
  String name;
  String dateOfBirth;
  dynamic registered;

  factory Member.fromRawJson(String str) => Member.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory Member.fromJson(Map<String, dynamic> json) => Member(
        id: json["id"] == null ? null : json["id"],
        name: json["name"] == null ? null : json["name"],
        dateOfBirth: json["dateOfBirth"] == null ? null : json["dateOfBirth"],
        registered: json["registered"],
      );

  static Future<List<Member>> getDataMember() async {
    final apiUrl = Uri.parse('$IP_SERVER/member/list');
    print(dataUser.partnerCode);
    final data = await http.post(apiUrl, body: {
      "partnerID": "${dataUser.partnerId}",
      "partnerCode": "${dataUser.partnerCode}"
    });
    var jsonObject = jsonDecode(data.body);
    var getData = jsonObject['members'];
    return getData
        .map<Member>((json) => Member(
              id: json["id"] == null ? null : json["id"],
              name: json["name"] == null ? null : json["name"],
              dateOfBirth:
                  json["dateOfBirth"] == null ? null : json["dateOfBirth"],
              registered: json["registered"],
            ))
        .toList();
  }

  static Future<String> createMember(Member dataMember) async {
    final apiUrl = Uri.parse('$IP_SERVER/member/add');
    print("masuk");
    final data = await http.post(apiUrl, body: {
      "partnerID": "${dataUser.partnerId}",
      "partnerCode": "${dataUser.partnerCode}",
      "name": "${dataMember.name}",
      "dateOfBirth": "${dataMember.dateOfBirth}",
    });
    print(jsonDecode(data.body));
    var jsonObject = jsonDecode(data.body);
    var getData = jsonObject['status'];
    print(getData);
    return getData;
  }

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "name": name == null ? null : name,
        "dateOfBirth": dateOfBirth == null ? null : dateOfBirth,
        "registered": registered,
      };
}
