import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:k24/view/login/login.dart';
import 'package:k24/view/member/homeMember.dart';
import 'package:k24/viewmodel/addMember/add_member_bloc.dart';
import 'package:k24/viewmodel/homeMember/home_member_bloc.dart';
import 'package:k24/viewmodel/login/login_bloc.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider<HomeMemberBloc>(
          create: (context) => HomeMemberBloc(),
        ),
        BlocProvider<AddMemberBloc>(
          create: (context) => AddMemberBloc(),
        ),
        BlocProvider<LoginBloc>(
          create: (context) => LoginBloc(),
        ),
      ],
      child: MaterialApp(
        title: 'K24',
        theme: ThemeData(
          primarySwatch: Colors.green,
        ),
        home: BlocBuilder<LoginBloc, LoginState>(
          builder: (context, state) {
            if (state is memberView) {
              return homeMember();
            } else {
              return loginPage();
            }
          },
        ),
      ),
    );
  }
}
