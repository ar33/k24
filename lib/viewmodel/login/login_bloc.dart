import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:k24/model/login.dart';
import 'package:k24/model/user.dart';
import 'package:meta/meta.dart';
import 'package:k24/setting.dart';

part 'login_event.dart';

part 'login_state.dart';

class LoginBloc extends Bloc<LoginEvent, LoginState> {
  LoginBloc() : super(LoginInitial());

  @override
  Stream<LoginState> mapEventToState(
    LoginEvent event,
  ) async* {
    // TODO: implement mapEventToState
    if (event is postLogin) {
      User data;
      data = await User.postLogin(event.dataUser);
      print("oke");
      print(data.name);
      dataUser = data;
      yield memberView();
    }
    if (event is logout) {
      yield loginView();
    }
  }
}
