part of 'login_bloc.dart';

@immutable
abstract class LoginEvent {
  LoginApi dataUser;
}

class postLogin extends LoginEvent {
  LoginApi dataUser;

  postLogin(this.dataUser);
}

class logout extends LoginEvent {
  logout();
}
