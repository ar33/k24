part of 'home_member_bloc.dart';

@immutable
abstract class HomeMemberState {
  List<Member> dataMember;

  HomeMemberState({this.dataMember});
}

class HomeMember extends HomeMemberState {
  List<Member> dataMember;

  HomeMember(this.dataMember);
}

class HomeMemberInitial extends HomeMemberState {}
