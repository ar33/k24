import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:k24/model/member.dart';
import 'package:meta/meta.dart';

part 'home_member_event.dart';

part 'home_member_state.dart';

class HomeMemberBloc extends Bloc<HomeMemberEvent, HomeMemberState> {
  HomeMemberBloc() : super(HomeMemberInitial());

  @override
  Stream<HomeMemberState> mapEventToState(
    HomeMemberEvent event,
  ) async* {
    // TODO: implement mapEventToState
    if (event is getDataMember) {
      List<Member> dataMember;
      dataMember = await Member.getDataMember();
      yield HomeMember(dataMember);
    } else if (event is clear) {
      yield HomeMemberInitial();
    }
  }
}
