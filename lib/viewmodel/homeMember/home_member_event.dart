part of 'home_member_bloc.dart';

@immutable
abstract class HomeMemberEvent {}

class getDataMember extends HomeMemberEvent {}

class clear extends HomeMemberEvent {}
