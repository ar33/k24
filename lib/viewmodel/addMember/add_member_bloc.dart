import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:k24/model/member.dart';
import 'package:k24/viewmodel/homeMember/home_member_bloc.dart';
import 'package:meta/meta.dart';

part 'add_member_event.dart';

part 'add_member_state.dart';

class AddMemberBloc extends Bloc<AddMemberEvent, AddMemberState> {
  AddMemberBloc() : super(AddMemberInitial());

  @override
  Stream<AddMemberState> mapEventToState(
    AddMemberEvent event,
  ) async* {
    // TODO: implement mapEventToState

    if (event is createMember) {
      String status = 'gagal simpan';
      status = await Member.createMember(event.dataMember);
      print(status);
      if (status == 'success') {
        yield successPost();
      } else {
        yield failedPost();
      }
    }
    if (event is clear) {
      yield AddMemberInitial();
    }
  }
}
