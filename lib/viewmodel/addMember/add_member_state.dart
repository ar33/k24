part of 'add_member_bloc.dart';

@immutable
abstract class AddMemberState {
  String status = '';
}

// ignore: must_be_immutable
class AddMemberInitial extends AddMemberState {}

class successPost extends AddMemberState {
  successPost();
}

class failedPost extends AddMemberState {
  failedPost();
}
