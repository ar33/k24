part of 'add_member_bloc.dart';

@immutable
abstract class AddMemberEvent {}

class createMember extends AddMemberEvent {
  Member dataMember;

  createMember(this.dataMember);
}

class clear extends AddMemberEvent {
  clear();
}
