import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter/widgets.dart';
import 'package:k24/model/login.dart';
import 'package:k24/model/user.dart';
import 'package:k24/setting.dart';
import 'package:k24/viewmodel/login/login_bloc.dart';
import 'package:toast/toast.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

enum _answer { ulang, ok }

final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

class loginPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _loginPage();
  }
}

class _loginPage extends State<loginPage> {
  DateTime currentBackPressTime;
  LoginBloc loginBloc;
  TextEditingController username = TextEditingController();
  TextEditingController password = TextEditingController();

  @override
  Widget build(BuildContext context) {
    loginBloc = BlocProvider.of<LoginBloc>(context);
    return Scaffold(
        resizeToAvoidBottomInset: false,
        key: _scaffoldKey,
        backgroundColor: Colors.green,
        body: WillPopScope(
          onWillPop: _onWillPop,
          child: Stack(
            children: [
              Column(
                children: [
                  Container(
                    height: 30,
                  ),
                  Image.asset(
                    "images/logo_k24.jpg",
                    height: 250,
                  ),
                ],
              ),
              // LOGIN SHEET
              Column(
                children: [
                  Container(
                    height: 60,
                  ),
                  Container(
                    margin: EdgeInsets.only(left: 10, right: 10, top: 230),
                    height: 250,
                    padding: EdgeInsets.all(20),
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(6),
                        border: Border.all(color: Colors.grey[300], width: 1)),
                    child: Column(
                      children: [
                        Container(
                          padding: EdgeInsets.only(left: 0, right: 0),
                          child: TextField(
                            controller: username,
                            style: TextStyle(
                                fontSize: 20, color: Colors.grey[700]),
                            decoration: InputDecoration(
                                hintStyle: TextStyle(
                                    fontWeight: FontWeight.normal,
                                    fontSize: 20),
                                hintText: "Username",
                                enabledBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(4),
                                  borderSide: BorderSide(
                                    color: Colors.grey[200],
                                    width: 1,
                                  ),
                                ),
                                border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(4),
                                  borderSide: BorderSide(
                                    color: Colors.grey[200],
                                    width: 1,
                                  ),
                                ),
                                focusedBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(4),
                                  borderSide: BorderSide(
                                    color: Colors.grey[200],
                                    width: 1,
                                  ),
                                ),
                                prefixIcon: Padding(
                                  child: IconTheme(
                                    data: IconThemeData(color: Colors.blue),
                                    child: Icon(Icons.account_circle),
                                  ),
                                  padding: EdgeInsets.only(left: 5, right: 5),
                                )),
                          ),
                        ),
                        SizedBox(
                          height: 5,
                        ),
                        Container(
                          padding: EdgeInsets.only(left: 0, right: 0),
                          child: TextField(
                            controller: password,
                            obscureText: true,
                            obscuringCharacter: "*",
                            style: TextStyle(fontSize: 20, color: Colors.grey),
                            decoration: InputDecoration(
                                hintStyle: TextStyle(
                                    fontWeight: FontWeight.normal,
                                    fontSize: 20),
                                hintText: "Password",
                                enabledBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(4),
                                  borderSide: BorderSide(
                                    color: Colors.grey[200],
                                    width: 1,
                                  ),
                                ),
                                focusedBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(4),
                                  borderSide: BorderSide(
                                    color: Colors.grey[200],
                                    width: 1,
                                  ),
                                ),
                                border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(4),
                                  borderSide: BorderSide(
                                    color: Colors.grey[200],
                                    width: 1,
                                  ),
                                ),
                                prefixIcon: Padding(
                                  child: IconTheme(
                                    data: IconThemeData(color: Colors.blue),
                                    child: Icon(Icons.lock),
                                  ),
                                  padding: EdgeInsets.only(left: 5, right: 5),
                                )),
                          ),
                        ),
                        Spacer(),
                        Container(
                          alignment: Alignment.bottomRight,
                          height: 40,
                          // color: Colors.black,
                          child: CupertinoButton(
                            // color: COLORAPPBAR,
                            borderRadius: BorderRadius.circular(50),
                            padding: EdgeInsets.only(right: 40, left: 40),
                            child: Text(
                              "Login",
                              style: TextStyle(
                                  color: Colors.blue[500],
                                  fontWeight: FontWeight.bold),
                            ),
                            onPressed: () async {
                              await loginBloc.add(postLogin(LoginApi(
                                  nama: username.text,
                                  password: password.text)));
                              _scaffoldKey.currentState
                                  .showSnackBar(new SnackBar(
                                duration: new Duration(seconds: 5),
                                content: new Row(
                                  children: <Widget>[
                                    new CircularProgressIndicator(),
                                    new Text("  Signing-In..."),
                                  ],
                                ),
                              ));
                              // await Future.delayed(
                              //     Duration(milliseconds: 4500));
                            },
                          ),
                        )
                      ],
                    ),
                  ),
                ],
              )
            ],
          ),
        ));
  }

  Future<bool> _onWillPop() {
    DateTime now = DateTime.now();
    if (currentBackPressTime == null ||
        now.difference(currentBackPressTime) > Duration(seconds: 2)) {
      currentBackPressTime = now;
      Toast.show("Tap back again to leave", context,
          backgroundColor: Colors.grey.withOpacity(0.8),
          backgroundRadius: 3,
          duration: 2,
          gravity: Toast.CENTER);
      return Future.value(false);
    }
    return Future.value(true);
  }
}
