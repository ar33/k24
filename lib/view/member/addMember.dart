import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'dart:async';
import 'package:intl/intl.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:k24/model/member.dart';
import 'package:k24/viewmodel/addMember/add_member_bloc.dart';

enum AnswerDialog { ok, batal }

class addMember extends StatefulWidget {
  @override
  _addMemberState createState() => _addMemberState();
}

class _addMemberState extends State<addMember> {
  DateTime selectedDate = DateTime.now();
  AddMemberBloc addMemberBloc;
  TextEditingController namaMember = TextEditingController();
  final DateFormat format = DateFormat('yyyy-MM-dd');

  Future<void> _selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: selectedDate,
        firstDate: DateTime(2015, 8),
        lastDate: DateTime(2101));
    if (picked != null && picked != selectedDate)
      setState(() {
        selectedDate = picked;
      });
  }

  @override
  Widget build(BuildContext context) {
    String tanggal = format.format(selectedDate);
    addMemberBloc = BlocProvider.of<AddMemberBloc>(context);
    return Scaffold(
      appBar: AppBar(
        title: Text("Add Member"),
      ),
      body: Container(
        padding: EdgeInsets.all(10),
        child: Form(
          child: Column(
            children: [
              TextFormField(
                maxLength: 18,
                controller: namaMember,
                keyboardType: TextInputType.name,
                maxLines: null,
                validator: (value) {
                  if (value.isEmpty) {
                    return 'Wajib di isi';
                  }
                  return null;
                },
                decoration: InputDecoration(
                  labelText: "Nama Member",
                ),
                onChanged: (value) {},
              ),
              SizedBox(
                height: 20,
              ),
              Container(
                height: 40,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "Tanggal lahir",
                      style: TextStyle(color: Colors.green, fontSize: 12),
                    ),
                    Row(
                      children: [
                        Text(tanggal.toString()),
                        Spacer(),
                        InkWell(
                            onTap: () {
                              _selectDate(context);
                            },
                            child: Icon(Icons.calendar_today_outlined))
                      ],
                    ),
                  ],
                ),
              ),
              Divider(
                thickness: 1,
              ),
              SizedBox(
                height: 30,
              ),
              CupertinoButton(
                color: Colors.green,
                onPressed: () {
                  _showDialog(context: context);
                  addMemberBloc.add(createMember(Member(
                    name: namaMember.text.toString(),
                    dateOfBirth: tanggal.toString(),
                  )));
                },
                child: Text("save"),
              )
            ],
          ),
        ),
      ),
    );
  }

  Future<void> _showDialog({BuildContext context}) async {
    switch (await showDialog<AnswerDialog>(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return BlocBuilder<AddMemberBloc, AddMemberState>(
              builder: (context, state) {
            print(state.status);
            if (state is AddMemberInitial) {
              return Dialog(
                elevation: 0,
                backgroundColor: Colors.transparent,
                child: Container(
                    color: Colors.transparent,
                    width: 50,
                    height: 50,
                    child: Center(child: CircularProgressIndicator())),
              );
            } else if (state is successPost) {
              Future.delayed(const Duration(seconds: 2),
                  () => Navigator.pop(context, AnswerDialog.ok));
              return Dialog(
                elevation: 0,
                backgroundColor: Colors.white,
                child: Container(
                    color: Colors.transparent,
                    width: 50,
                    height: 150,
                    child: Column(
                      children: [
                        Container(
                          height: 10,
                        ),
                        Image.asset(
                          'images/transaksi_sukses.png',
                          height: 50,
                        ),
                        Center(child: Text("Sukses Simpan Data")),
                      ],
                    )),
              );
            } else {
              Future.delayed(const Duration(seconds: 2),
                  () => Navigator.pop(context, AnswerDialog.batal));
              return Dialog(
                elevation: 0,
                backgroundColor: Colors.white,
                child: Container(
                    color: Colors.transparent,
                    width: 50,
                    height: 50,
                    child: Center(child: Text("Gagal Simpan Data"))),
              );
            }
          });
        })) {
      case AnswerDialog.ok:
        namaMember.clear();
        addMemberBloc.add(clear());
        Navigator.pop(context);
        break;
      case AnswerDialog.batal:
        break;
    }
  }
}
