import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:k24/view/member/addMember.dart';
import 'package:k24/viewmodel/homeMember/home_member_bloc.dart';

class homeMember extends StatelessWidget {
  HomeMemberBloc homeMemberBloc;
  TextStyle headerList = TextStyle(fontWeight: FontWeight.bold);

  @override
  Widget build(BuildContext context) {
    homeMemberBloc = BlocProvider.of<HomeMemberBloc>(context);
    return BlocBuilder<HomeMemberBloc, HomeMemberState>(
        builder: (context, stateDataMember) {
      if (stateDataMember is HomeMemberInitial) {
        print("ambil data");
        homeMemberBloc.add(getDataMember());
        return Scaffold(
          appBar: AppBar(
            backgroundColor: Colors.transparent,
            elevation: 0,
            title: Text(
              "Member K24",
              style: TextStyle(color: Colors.green),
            ),
            actions: [
              IconButton(
                onPressed: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => addMember(),
                      ));
                },
                icon: Icon(Icons.add),
              )
            ],
          ),
          body: Container(
            child: Center(child: CircularProgressIndicator()),
          ),
        );
      } else {
        return Scaffold(
          backgroundColor: Colors.grey[300],
          appBar: AppBar(
            backgroundColor: Colors.transparent,
            elevation: 0,
            title: Text(
              "Member K24",
              style: TextStyle(color: Colors.green),
            ),
            actions: [
              IconButton(
                  onPressed: () {
                    Navigator.push(context, MaterialPageRoute(
                      builder: (context) {
                        return addMember();
                      },
                    )).then((value) => homeMemberBloc.add(clear()));
                  },
                  iconSize: 130,
                  icon: Text(
                    "Tambah Data",
                    style: TextStyle(
                        color: Colors.green, fontWeight: FontWeight.bold),
                  ))
            ],
          ),
          body: Container(
            margin: EdgeInsets.all(10),
            decoration: BoxDecoration(
                color: Colors.white,
                border: Border.all(color: Colors.grey[300], width: 1),
                borderRadius: BorderRadius.circular(6)),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  padding: EdgeInsets.all(10),
                  child: Row(
                    children: [
                      Expanded(flex: 10, child: Text("Id", style: headerList)),
                      Expanded(
                          flex: 60, child: Text("Nama", style: headerList)),
                      Expanded(
                          flex: 30,
                          child: Text("Tanggal Lahir", style: headerList)),
                    ],
                  ),
                ),
                Divider(
                  height: 1,
                  thickness: 1,
                ),
                Expanded(
                    child: ListView.separated(
                  separatorBuilder: (context, index) => Divider(
                    height: 1,
                    thickness: 1,
                    endIndent: 10,
                    indent: 10,
                  ),
                  padding: EdgeInsets.zero,
                  itemCount: stateDataMember.dataMember.length,
                  itemBuilder: (context, index) {
                    return Container(
                        padding: EdgeInsets.all(10),
                        child: Row(
                          children: [
                            Expanded(
                                flex: 10,
                                child: Text(
                                    "${stateDataMember.dataMember[index].id}")),
                            Expanded(
                                flex: 60,
                                child: Text(
                                    "${stateDataMember.dataMember[index].name}")),
                            Expanded(
                                flex: 30,
                                child: Text(
                                    "${stateDataMember.dataMember[index].dateOfBirth}")),
                          ],
                        ));
                  },
                ))
              ],
            ),
          ),
        );
      }
      // return Container(
      //   child: Center(child: CircularProgressIndicator()),
      // );
    });
  }
}
